package bill_with_patterns;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import bill_with_patterns.Tools;

public class Main {

	public static void main(String[] args) {
		System.out.println("Good morning Mr Danny");
		String currentDate = Tools.getCurrentTime();
		System.out.println("Today is " + currentDate);
		
        //Scanner scanner = new Scanner(System. in);
        
        //System.out.print("MySQL User: "); 
        //String mysql_user = scanner.nextLine();
        String mysql_user = "azapat47";
        
        //System.out.print("MySQL Password: "); 
        //String mysql_password = scanner.nextLine();
        String mysql_password = "abcDEF123*";
        
        // This should be retrieved from MySQL Database, Not enough time
        ItemType type = new ItemType(1,"Comidas");
        Item item = new Item(1,"Perrito",3000,type);
        Customer customer = new Customer(1,"Pepito", "Perez Potato", "Male", "1999-03-16", "soltero");
        DAOBillJDBC dao_bill = DAOBillJDBC.getInstance(mysql_user, mysql_password);
        Bill bill = dao_bill.createBill(customer);
        dao_bill.connection.execute("insert into ItemType (itemtype_description) values (\"adiciones\");");
        //Bill bill = new Bill(customer);
        System.out.println(bill);
	}
}
