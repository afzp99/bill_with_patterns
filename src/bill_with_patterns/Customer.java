package bill_with_patterns;

public class Customer {
	private int id;
	private String name;
	private String lastname;
	private String gender;
	private String birthday;
	private String civilState;
	
	public Customer(int id, String name, String lastname,String gender,
			String birthday, String civilState) {
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.gender = gender;
		this.birthday = birthday;
		this.civilState = civilState;
	}
	
	public int getId() {
		return id;
	}
	
	public String toString() {
		return "(customer_name, customer_lastname, gender, birthday, civilState) values (" +
				name + "," + lastname + "," + gender  + "," + birthday + "," + civilState + ")";
	}
}
