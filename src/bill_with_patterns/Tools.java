package bill_with_patterns;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Tools {
	
	private static String DATABASE_NAME = "bill_azapat47"; 
	
	public static String getCurrentTime() {
	    Date date = new Date();
	    String strDateFormat = "yyyy-MM-dd";
	    DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
	    String formattedDate= dateFormat.format(date);
	    return formattedDate;
	}
	
	public static String getDBName() {
		return DATABASE_NAME;
	}
	
	public static int getResultSetSize(ResultSet set) {
		int count = 0;
		try {
			if (set.last()) {
				count = set.getRow();
				set.beforeFirst();
			}
		} catch (SQLException e) {
			System.err.println("ERROR: Couldn't manipulate ResultSet");
			e.printStackTrace();
		}
		return count;
	}
}
