package bill_with_patterns;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL_Connection {
	private static MySQL_Connection singleton;
	
	private Connection connection;
	
	public static MySQL_Connection getInstance(String user, String passwd) {
		if (singleton == null) {
			singleton = new MySQL_Connection(user, passwd);
		} return singleton;
	}
	
	public void closeConnection() {
		try {
			this.connection.close();
		} catch (SQLException e) {
			System.err.println("ERROR:Couldn't close connection...");
			e.printStackTrace();
		}
	}
	
	public void finalize() {
		this.closeConnection();
	}
	
	private MySQL_Connection(String user, String passwd) {
		try {
		DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
		connection = DriverManager.getConnection(  
		"jdbc:mysql://localhost:3306/" + Tools.getDBName(),user,passwd);  
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public ResultSet executeQuery(String query) {
		ResultSet set = null;
		try {
			Statement stmt = this.connection.createStatement();  
			set = stmt.executeQuery("select * from ItemType");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return set;
	}
	
	public void execute(String query) {
		ResultSet set = null;
		try {
			Statement stmt = this.connection.createStatement();  
			stmt.execute("select * from ItemType");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
