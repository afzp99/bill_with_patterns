package bill_with_patterns;

public class ItemType {
	private int id;
	private String description;
	
	public ItemType(int id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public String toString() {
		return id + "" + description;
	}
}
