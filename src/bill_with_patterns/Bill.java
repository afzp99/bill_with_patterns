package bill_with_patterns;

import java.util.LinkedList;
import bill_with_patterns.Tools;

public class Bill {
	private int id;
	private String date;
	private Customer customer;
	private int total;
	private String state;
	private LinkedList<Item> items;
	
	public Bill(Customer customer) {
		this.id = 1;
		this.customer = customer;
		this.date = Tools.getCurrentTime();
		this.total = 0;
		this.state = "Empty";
		this.items = new LinkedList<Item>();
	}
	
	public String getDate() {
		return date;
	}
	
	public void addItem(Item item) {
		this.items.add(item);
	}
	
	public String valueObject() {
		return "(date, customer_id) values (" +
				this.date + "," + this.customer.getId() + ")";
	}
	
	public String toString() {
		String customer_info = customer.toString();
		String items = "";
		for (int i = 0; i < this.items.size(); i++) {
		    items += this.items.get(i).toString();
		}
		String bill_descrip = customer_info + items + "| State: " + state +
				" | Total:" + total;
		return bill_descrip;
	}
}
