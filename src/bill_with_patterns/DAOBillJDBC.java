package bill_with_patterns;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bill_with_patterns.DAOBill;
import bill_with_patterns.Tools;

public class DAOBillJDBC implements DAOBill{
	public MySQL_Connection connection;
	
	private static DAOBillJDBC singleton;
	
	public static DAOBillJDBC getInstance(String user, String passwd) {
		if (singleton == null) {
			singleton = new DAOBillJDBC(user, passwd);
		} return singleton;
	}
	
	private DAOBillJDBC(String user, String passwd) {
		connection = MySQL_Connection.getInstance(user, passwd);
	}
	
	@Override
	public Bill createBill(Customer customer) {
		Bill bill = new Bill(customer);
		connection.executeQuery("insert into Bill " + bill.valueObject() + ";");
		return bill;
	}

	@Override
	public void addItem(Item item) {
		// TODO Auto-generated method stub
	}

    public ItemType[] getItemTypes(){
    	try{  
    		String query_get_items = "select * from ItemType";
    		ResultSet set = this.connection.executeQuery(query_get_items);
    		int n_results = Tools.getResultSetSize(set);
    		ItemType itemTypes[] = new ItemType[n_results];
    		int i = 0;
    		while(set.next()) {
    			String description = set.getString(2);
    			int id = set.getInt(1);
    			ItemType itemType = new ItemType(id, description);
    			itemTypes[i] = itemType;
    			System.out.println(itemType);
    			i++;
    		}
    		return itemTypes;
    	} catch(Exception e){ 
    		System.err.println(e);
    		return null;
   		}
    }

	@Override
	public void removeBill() {
		// TODO Auto-generated method stub
	}

}
