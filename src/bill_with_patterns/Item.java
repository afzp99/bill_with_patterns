package bill_with_patterns;

public class Item {
	private int id;
	private String description;
	private int unitValue;
	private ItemType itemType;
	
	public Item(int id, String description, int unitValue, ItemType itemType) {
		this.id = id;
		this.description = description;
		this.unitValue = unitValue;
		this.itemType = itemType;
	}
	
	public String toString() {
		return "Item:" + this.description + 
			" - UnitValue:" + this.unitValue +
			" - ItemType:" + this.itemType.toString();
	}
}
