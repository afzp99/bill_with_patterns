package bill_with_patterns;

public interface DAOBill {
	public Bill createBill(Customer customer);
	public void addItem(Item item);
	public void removeBill();
}
