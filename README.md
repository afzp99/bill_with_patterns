# Bill with Patterns

Small Demo in Java that uses some design patterns in a simple CRUD for a bill creation (and its persistence in a DB)

## Usage ##

**1)** This is an Eclipse project, so in order to compile it without problems, import it as an Eclipse Project.  

**2)** Check if the library com.mysql is present in your system, otherwise, use the mysql-connector.jar present in the
lib/ folder and import it to classpath in Eclipse.  

**3)** Load the bill_azapat47 database, with its tables and initial registers executing:

    $ mysql -u user -p < bill_azapat47_database.sql

## Structure ##

![image info](./class_diagram.png)
